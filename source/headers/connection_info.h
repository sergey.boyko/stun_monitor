#pragma once
#include <string>
struct connection_info
{
	std::string server_ip;
	int server_port;
	int index_Connection;
	std::string node_ip;
	std::string returned_ip_port;
	bool stun_server_is_active;
};