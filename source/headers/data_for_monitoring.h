#pragma once
#include <string>
#include <vector>

struct stun_node
{
	std::string node_ip;
	std::vector<std::pair<std::string, int>> stun_list;
};

struct data_for_monitoring
{
	std::vector<stun_node> node_list;
	int period_sending_request_ms;
	int max_number_request_sent;
};