#!/usr/bin/env python3
import smtplib
from email.mime.text import MIMEText
import os
import json
import argparse


def parse_command_line():
    parser = argparse.ArgumentParser()

    parser.add_argument('--abs_path', action='store', dest='abs_path', type=str,
                        help='Absolute path to installed directory')
    parser.add_argument('--exe', action='store', dest='exe_path', type=str, help='Path to executable file')
    parser.add_argument('--config', action='store', dest='config_path', type=str,
                        help='Path to the executable configuration file')
    parser.add_argument('--res_old', action='store', dest='result_old', type=str, help='Path to old file with results')
    parser.add_argument('--res_new', action='store', dest='result_new', type=str, help='Path to new file with results')
    parser.add_argument('--err', action='store', dest='error_log', type=str, help='Path to error log')

    parser.add_argument('--from_address', action='store', dest='from_address', type=str,
                        help='Mail form which the message will be sent')
    parser.add_argument('--to_address', action='store', dest='to_address', type=str,
                        help='Mail to which the message will be sent')

    args = parser.parse_args()

    args.exe_path = args.abs_path + './' + args.exe_path
    args.config_path = args.abs_path + args.config_path
    args.result_old = args.abs_path + args.result_old
    args.result_new = args.abs_path + args.result_new
    args.error_log = args.abs_path + args.error_log

    return args


def send_message(message, from_address, to_address):
    msg = initialization_message(message, from_address, to_address)

    s = smtplib.SMTP('localhost')
    s.sendmail(from_address, [to_address], msg.as_string())
    s.quit()


def initialization_message(message, from_address, to_address):
    msg = MIMEText(message, 'plain', 'utf-8')
    msg['Subject'] = 'stun_monitor'
    msg['From'] = from_address
    msg['To'] = to_address

    return msg


def get_str_file(file):
    text = ''
    if os.path.exists(file):
        with open(file) as data:
            text = data.read()
    else:
        return False
    return text


def get_json(file):
    data = ''
    if os.path.exists(file):
        try:
            with open(file) as json_data:
                data = json.load(json_data)
        except ValueError:
            return False
    else:
        return False
    return data


class data_analyzer:
    def __init__(self, _executable_file, _config_path, _result_new, _result_old, _error_log):
        self.__executable_file = _executable_file
        self.__config_path = _config_path
        self.__result_new = _result_new
        self.__result_old = _result_old
        self.__error_log = _error_log

        if os.path.exists(_result_new):
            os.remove(_result_new)
        self.__command = _executable_file + ' ' + '--config_path=' + _config_path + \
                         ' ' + '--record_path=' + _result_new + ' ' + '2>' + _error_log

    def start_data_analysis(self):
        message = ''

        if os.system(self.__command) == 0:
            if os.path.exists(self.__result_old):
                message = self.__data_analysis__()
            self.__end_of_analysis__()
        else:
            message = 'Monitor errors log: ' + get_str_file(self.__error_log)

        return message

    def __end_of_analysis__(self):
        if os.path.exists(self.__result_old):
            os.remove(self.__result_old)
        os.rename(self.__result_new, self.__result_old)

    def __data_analysis__(self):
        data_old = get_json(self.__result_old)
        data_new = get_json(self.__result_new)
        message = ''

        if data_old == False or data_old == False:
            message = 'data_analysis.py: Could not open ' + self.__result_old
            message += ' or ' + self.__result_new
            return message

        invalid_servers = dict()

        try:
            for node_new in data_new:
                for stun_new in node_new["stuns"]:
                    if stun_new["IsActive"] == 'No':
                        stun_old = self.__get_stun(data_old, node_new["node"], stun_new["IP"])
                        if stun_old:
                            if stun_old["IsActive"] == 'No':
                                if node_new["node"] not in invalid_servers:
                                    invalid_servers[node_new["node"]] = list()
                                invalid_servers[node_new["node"]].append(stun_new["IP"])

            if len(invalid_servers) > 0:
                message = 'Non-working servers:\n'
                for node, stuns in invalid_servers.items():
                    message+='\n'
                    message += 'node: ' + node + '\n'
                    message += 'stuns: \n'
                    for stun in stuns:
                        message+='\r'+stun + '\n'


        except KeyError:
            message = 'data_analysis.py: Invalid monitoring result'

        return message

    def __get_stun(self, data, node_ip, stun_ip):
        for node in data:
            if node["node"] == node_ip:
                for stun in node["stuns"]:
                    if stun["IP"] == stun_ip:
                        return stun
        return False


args = parse_command_line()
if args:

    analyzer = data_analyzer(args.exe_path, args.config_path, args.result_new, args.result_old, args.error_log)
    message = analyzer.start_data_analysis()

    if message:
        send_message(message, args.from_address, args.to_address)
else:
    print('script.py: Could not parse command line')
