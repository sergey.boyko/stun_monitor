#include "headers/requester.h"
#include <iostream>
#include <cstring>
#include <memory>

requester::requester()
{
	_curl = curl_easy_init();
}

int requester::read_handle( char *data, size_t size, size_t nmemb, std::string *buffer )
{
	int res;
	res = size * nmemb;
	buffer->append(data, res);
	return res;
}

bool requester::get_page( const parsing_config &config )
{
	curl_easy_setopt(_curl, CURLOPT_URL, config.url_give_servers.c_str());
	curl_easy_setopt(_curl, CURLOPT_HEADER, 1);
	curl_easy_setopt(_curl, CURLOPT_WRITEFUNCTION, requester::read_handle);
	curl_easy_setopt(_curl, CURLOPT_WRITEDATA, &_buffer);
	_result = curl_easy_perform(_curl);

	if ( _result == CURLE_OK )
	{
		std::istringstream is(_buffer);

		std::string http_version;
		std::string status_message;
		unsigned int status_code;

		is >> http_version;
		is >> status_code;
		std::getline(is, status_message);

		//Check the correctness of the response
		if ( !is || http_version.find("HTTP") != 0 )
		{
			std::cerr << __PRETTY_FUNCTION__ << ":" << __LINE__ << ": Invalid response" << std::endl;
			std::cerr << "Page:" << std::endl << _buffer << std::endl;
			return false;
		}

		std::string header;

		//Remove superfluous
		while ( std::getline(is, header) && header != "\r" )
		{

		}

		try
		{
			is >> _root;
		}
		catch ( const Json::RuntimeError &er )
		{
			std::cerr << __PRETTY_FUNCTION__ << ":" << __LINE__ << ": Invalid download page" << std::endl;
			std::cerr << "Page:" << std::endl << _buffer << std::endl;
			return false;
		}
		catch ( const std::exception &ex )
		{
			std::cerr << __PRETTY_FUNCTION__ << ":" << __LINE__ << ": Invalid download page" << std::endl;
			std::cerr << "Page:" << std::endl << _buffer << std::endl;
			return false;
		}
		return true;
	}

	std::cerr << __PRETTY_FUNCTION__ << ":" << __LINE__ << ": Could't get page" << std::endl;
	return false;
}

bool requester::request( const parsing_config &config, data_for_monitoring &data_for_monitoring_ )
{
	if ( _curl != nullptr )
	{
		if ( !get_page(config))
		{
			return false;
		}
		if ( !response_analysis(data_for_monitoring_))
		{
			return false;
		}
	}
	else
	{
		std::cerr << __PRETTY_FUNCTION__ << ":" << __LINE__ << ": Could't open cURL" << std::endl;
		std::cerr << "Page:" << std::endl << _buffer << std::endl;
		return false;
	}

	data_for_monitoring_.max_number_request_sent = config.max_number_request_sent;
	data_for_monitoring_.period_sending_request_ms = config.period_sending_request_ms;

	return true;
}

bool requester::response_analysis( data_for_monitoring &data_for_monitoring_ )
{
	if ( !_root.isArray())
	{
		std::cerr << __PRETTY_FUNCTION__ << ":" << __LINE__
		          << ": The downloaded file does not contain an array of servers" << std::endl;
		std::cerr << "Page: " << std::endl << _buffer << std::endl << std::endl;
		return false;
	}

	for ( auto &j_node: _root )
	{
		try
		{
			data_for_monitoring_.node_list.emplace_back();
			data_for_monitoring_.node_list.rbegin()->node_ip = j_node["node"].asString();

			if ( !j_node["stuns"].isArray())
			{
				std::cerr << __PRETTY_FUNCTION__ << ":" << __LINE__
				          << ": The downloaded file does not contain an array of servers" << std::endl;
				std::cerr << "Page: " << std::endl << _buffer << std::endl << std::endl;
				return false;
			}

			for ( auto j_stun:j_node["stuns"] )
			{
				if ( !j_stun.isMember("ip") || !j_stun.isMember("port"))
				{
					std::cerr << __PRETTY_FUNCTION__ << ":" << __LINE__ << ": not found ip or port" << std::endl;
					return false;
				}

				data_for_monitoring_.node_list.rbegin()->stun_list.emplace_back(j_stun["ip"].asString(), j_stun["port"].asInt());
			}
		}
		catch ( const Json::LogicError &er )
		{
			std::cerr << __PRETTY_FUNCTION__ << ":" << __LINE__ << ": " << er.what() << std::endl;
			return false;
		}
		catch ( const std::exception &ex )
		{
			std::cerr << __PRETTY_FUNCTION__ << ":" << __LINE__ << std::endl;
			return false;
		}
	}

	return true;
}

requester::~requester()
{
	curl_easy_cleanup(_curl);
}
